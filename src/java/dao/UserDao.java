/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Admin;
import model.Guest;
import model.Role;
import model.User;

/**
 *
 * @author jumpman
 */
public class UserDao {
    
    
     public static void saveUser(String username,String password,String firstName,String lastName,String gender,String age,String phone,String role){
        String insert_query="INSERT INTO user "
                + "(username,password,firstname,lastname,gender,age,phone,role)"
                + "VALUES(?,?,?,?,?,?,?,?)";
        
        try(Connection con=getConnected()){
            PreparedStatement statement=con.prepareStatement(insert_query);
	    String pass = hashPassword(password,age);
            statement.setString(1,username);
            statement.setString(2,pass);
            statement.setString(3,firstName);
            statement.setString(4,lastName);
            statement.setString(5,gender);
            statement.setString(6,age);
            statement.setString(7,phone);
            statement.setString(8,role);
            statement.executeUpdate();
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
     
     /**
     * @param imageId
     * @param keyId
     * @author jumpman
     */
     public static void saveEncryption(String imageId ,String keyId){
        String insert_query="INSERT INTO encrypty "
                + "(imageId,keyId)"
                + "VALUES(?,?)";
        
        try(Connection con=getConnected()){
            PreparedStatement statement=con.prepareStatement(insert_query);
            statement.setString(1,imageId);
            statement.setString(2,keyId);
            statement.executeUpdate();
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    public static Boolean checkDecryption(String imageId,String keyId){
	 String select_query="SELECT * FROM encrypty WHERE imageId=? AND keyId=?";
        
        try(Connection con=getConnected()){
            PreparedStatement statement=con.prepareStatement(select_query);
             statement.setString(1,imageId);
             statement.setString(2,keyId);
	     ResultSet result=statement.executeQuery();
	     while(result.next()){
		 String imgId=result.getString(1);
		 String key=result.getString(2);
		 if(imgId.equals(imageId)&&key.equals(keyId))return true;
		 
	     }
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }catch(Exception ex){
            ex.printStackTrace();
        }
	return false;
    }
    private static String getUserAge(String username){
	 String select_query="SELECT age FROM user WHERE username=? ";
        
        try(Connection con=getConnected()){
            PreparedStatement statement=con.prepareStatement(select_query);
             statement.setString(1,username);
	     ResultSet result=statement.executeQuery();
	     while(result.next()){
		String age=result.getString(1);
		return age;
	     }
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }catch(Exception ex){
            ex.printStackTrace();
        }
	return "";
    }
  
     public static User findUser(String username,String password){
	   User user=null;
        String select_query="SELECT * FROM user where username=? AND password=?";
        try(Connection con=getConnected()){
             PreparedStatement statement=con.prepareStatement(select_query);
	     String pass = hashPassword(password,getUserAge(username));
             statement.setString(1,username);
             statement.setString(2,pass);
            ResultSet result=statement.executeQuery();
            while(result.next()){
                 String userName=result.getString(1);
                String passwrd=result.getString(2);
                String firstName=result.getString(3);
                String lastName=result.getString(4);
                String gender=result.getString(5);
                String age=result.getString(6);
                String phone=result.getString(7);
                String role=result.getString(8);
		if(role.equals("GUEST")){
		    String plainPassword = reverseHashed(passwrd);
		    if(password.equals(plainPassword)){
			user=new Guest(userName, plainPassword, firstName, lastName, age,gender, phone, Role.valueOf(role));
		    }
		}else if (role.equals("ADMIN")){
		    String plainPassword = reverseHashed(passwrd);
		    if(password.equals(plainPassword)){
			user=new Guest(userName, plainPassword, firstName, lastName, age,gender, phone, Role.valueOf(role));
		    }
		}
            }
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return user;
     }
     
     public static User findByUsername(String username){
	   User user=null;
        String select_query="SELECT * FROM user where username=?";
        try(Connection con=getConnected()){
             PreparedStatement statement=con.prepareStatement(select_query);
	     
             statement.setString(1,username);
            ResultSet result=statement.executeQuery();
            while(result.next()){
                 String userName=result.getString(1);
                String passwrd=result.getString(2);
                String firstName=result.getString(3);
                String lastName=result.getString(4);
                String gender=result.getString(5);
                String age=result.getString(6);
                String phone=result.getString(7);
                String role=result.getString(8);
		if(role.equals("GUEST")){
		    String plainPassword = reverseHashed(passwrd);
		    
			user=new Guest(userName, plainPassword, firstName, lastName, age,gender, phone, Role.valueOf(role));
		}else if (role.equals("ADMIN")){
		    String plainPassword = reverseHashed(passwrd);
			user=new Guest(userName, plainPassword, firstName, lastName, age,gender, phone, Role.valueOf(role));
		}
            }
            
        }catch(SQLException ex){
            ex.printStackTrace();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return user;
     }
    
     private static Connection getConnected () throws Exception{
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        String url="jdbc:mysql://localhost:3306/users";
        String username="root";
        String password="";
        Connection con=DriverManager.getConnection(url, username, password);
        return con;
    }
     //the method reverses the string and append two stars front and back plus the age of the user
      private static String hashPassword(String password, String age) {
	String end = "**";
	String symbols = age + "**";
	StringBuilder sb = new StringBuilder(password + end);
	sb.reverse();
	return sb.toString() + symbols;
    }

    //the method returns back the hashed password to normal plain text.
    private static String reverseHashed(String password) {
	StringBuilder sb = new StringBuilder(password);
	String hashed = sb.reverse().toString();
	String removeSymbols = hashed.substring(4, hashed.length() - 2);
	return removeSymbols;
    }
}
