/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.UserDao;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
;



/**
 *
 * @author jumpman
 */
@WebServlet(name = "EncryController", urlPatterns = {"/encrypt"})
@MultipartConfig
public class EncryController extends HttpServlet {
    Map<String,String>error;
    String formKey=""; 
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	ServletFileUpload sf=new ServletFileUpload(new DiskFileItemFactory());
	List<FileItem>multifiles = null;
	try {
	    multifiles = sf.parseRequest(req);
	} catch (FileUploadException ex) {
	    Logger.getLogger(EncryController.class.getName()).log(Level.SEVERE, null, ex);
	}
	for(FileItem item:multifiles){
	    formKey=item.getString();
	}
	try{
	    if(validateKey(formKey)){


		for(FileItem item:multifiles){

		    try {

			String keyName=randomString();
			String imageId=keyName.concat(item.getName());
			item.write(new File("C:\\Users\\DolphiX People'S\\Music\\img_encrypt\\img\\"+imageId));
			encryption(imageId,formKey);
			UserDao.saveEncryption(imageId, formKey);
			req.getSession().setAttribute("success", "IMAGE encrypted as : "+imageId+" CHECK IT OUT AT... C:\\Users\\DolphiX People'S\\Music\\img_encrypt\\encryption");
			resp.sendRedirect("upload.jsp");

		    } catch (Exception ex) {

		    }
		}

	    }
	}catch(IllegalArgumentException ex){
		ex.getMessage();
		error.put("global", ex.getMessage());
		req.getSession().setAttribute("error", error);
		resp.sendRedirect("upload.jsp"); 
	}
	
    }	


    
     private void encryption(String image,String key) {
	System.out.println("staring encrypting......");
        try {
            FileInputStream fileInputStream = new FileInputStream("C:\\Users\\DolphiX People'S\\Music\\img_encrypt\\img\\"+image);
            FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\DolphiX People'S\\Music\\img_encrypt\\encryption\\"+image);
            byte k[] = key.getBytes();
            SecretKeySpec keySpec = new SecretKeySpec(k, "AES");
            Cipher enc = Cipher.getInstance("AES");
            enc.init(Cipher.ENCRYPT_MODE, keySpec);
            CipherOutputStream cipherOutputStream = new CipherOutputStream(fileOutputStream, enc);
            byte[] buf = new byte[1024];
            int read;
            while ((read = fileInputStream.read(buf)) != -1) {
                cipherOutputStream.write(buf, 0, read);
            }
            fileInputStream.close();
            fileOutputStream.flush();
            cipherOutputStream.close();
	    System.out.println("Encryption done..!!!!!");
        } catch (Exception e) {
	    
        }
    }
     
     
      private  String randomString() {

		String characters = "0123456789";
		String str = "";
		String createdEncryCode="ENCRY";
		char[] mynewCharacters = characters.toCharArray();
		Integer generatedCodeLength =4;
		for (int i = 0; i < generatedCodeLength; i++) {
		    int index = (int) (Math.random() *10);
		    String newString = characters.substring(index, characters.length() - 1);
		    str += mynewCharacters[newString.length()];
		}
		return createdEncryCode.concat(str);
    }
     
      
    private Boolean validateKey(String encryptionKey){
	error=new HashMap<>();
	
	if(encryptionKey.trim().isEmpty())error.put("keyIn","encryption key must be Entered");
	if(!encryptionKey.matches("[a-zA-Z0-9]{16}+"))error.put("key","key must be 16 characters of alphabets and numeric only");
	if(error.isEmpty()){
	    return true;
	}
	
	throw new IllegalArgumentException("please upload with the key");
	
    }
      
   
}
