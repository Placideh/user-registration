/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jumpman
 */
@WebServlet(name = "UserLogout", urlPatterns = {"/logout"})
public class UserLogout extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	HttpSession session=req.getSession();
	session.removeAttribute("success");
	session.removeAttribute("username");
	session.removeAttribute("error");
	session.removeAttribute("dsuccess");
	session.removeAttribute("errors");
	session.invalidate();
	resp.sendRedirect("login.jsp");
    }

   
}
