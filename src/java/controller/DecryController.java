/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.UserDao;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author jumpman
 */
@WebServlet(name = "DecryController", urlPatterns = {"/decrypt"})
@MultipartConfig
public class DecryController extends HttpServlet {
    Map<String,String>errors;
     String formKey=""; 

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	ServletFileUpload sf=new ServletFileUpload(new DiskFileItemFactory());
	List<FileItem>multifiles = null;
	try {
	    multifiles = sf.parseRequest(req);
	} catch (FileUploadException ex) {
	    Logger.getLogger(EncryController.class.getName()).log(Level.SEVERE, null, ex);
	}
	for(FileItem item:multifiles){
	    formKey=item.getString();
	}
	try{
	    if(validateKey(formKey)){
	
		for(FileItem item:multifiles){
		    
		    if(!UserDao.checkDecryption(item.getName(), formKey))throw new IllegalArgumentException("Invalid Encryption Image/key");
		  
		    try {
			decryption(item.getName(),formKey);
			req.getSession().setAttribute("dsuccess", "IMAGE: "+item.getName()+" DECRYPTED success CHECKOUT AT... C:\\Users\\DolphiX People'S\\Music\\img_encrypt\\decryption");
			resp.sendRedirect("upload.jsp");
		    } catch (Exception ex) {}
		    break;
		}
	    
	    }
	}catch(IllegalArgumentException ex){
		ex.getMessage();
		errors.put("global", ex.getMessage());
		req.getSession().setAttribute("errors", errors);
		resp.sendRedirect("upload.jsp"); 
	}
    }
    
     private static void decryption(String imageName,String key) {
	 System.out.println("Starting decrypting ......");
        try {
            FileInputStream fileInputStream = new FileInputStream("C:\\Users\\DolphiX People'S\\Music\\img_encrypt\\encryption\\"+imageName);
	    String imageDecrypt=imageName.substring(5);
	    String decrypt="DECRY".concat(imageDecrypt);
            FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\DolphiX People'S\\Music\\img_encrypt\\decryption\\"+decrypt);
            byte k[] =key.getBytes();
            SecretKeySpec keySpec = new SecretKeySpec(k, "AES");
            Cipher enc = Cipher.getInstance("AES");
            enc.init(Cipher.DECRYPT_MODE, keySpec);
            CipherOutputStream cipherOutputStream = new CipherOutputStream(fileOutputStream, enc);
            byte[] buf = new byte[1024];
            int read;

            while ((read = fileInputStream.read(buf)) != -1) {
                cipherOutputStream.write(buf, 0, read);
            }
            fileInputStream.close();
            fileOutputStream.flush();
            cipherOutputStream.close();
	    System.out.println("File Successfully Decrypted...!!!");
        } catch (Exception e) {
            System.out.println("Exception:"+e.getMessage());
        }
    }
      private Boolean validateKey(String encryptionKey){
	errors=new HashMap<>();
	
	if(encryptionKey.trim().isEmpty())errors.put("dkeyIn","decryption key must be Entered");
	if(!encryptionKey.matches("[a-zA-Z0-9]{16}+"))errors.put("key","key must 16 characters of alphabets and numeric only");
	if(errors.isEmpty()){
	    return true;
	}
	
	throw new IllegalArgumentException("please upload with the key");
	
    }

   
}
