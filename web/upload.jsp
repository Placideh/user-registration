<%-- 
    Document   : upload
    Created on : Dec 7, 2021, 10:28:57 PM
    Author     : jumpman
--%>

<%@page import="java.util.Map"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Encryption</title>
	<style>
	     body{
		font-family: Arial, Helvetica, sans-serif;
	    }
	     .container {
		background: #ddd;
		display: flex;
		justify-content: center;
		width: 60em;
		height: 43em;
		position: relative;
		left:18em;
		border-top:8px solid #fcc749;

	    }
	    .user{
		background: #f4f4f4;
		height:30px;
		width: 25%;
		padding:10px;
		position:fixed;
		left:18.7em;
	    }
	    
	    span {
		margin:5px;
		font-size:20px;
	    }
	       
	    input[type=text]{
		margin-bottom: 15px;
		width: 30em;
		height:3em;
	    }
	    #file{
		margin-bottom: 4px;
		width: 30em;
		height:2em;
	    }
	    
	    button {
		width: 7em;
		height:3.5em;
		margin-left: 5px;
	    }
	   .btnBack{
		margin:0;
		height:28px;
		margin-right:12px;
		width:6em;
		position:fixed;
		top:2.8em;
		right:45em;
		background:blue;
		border:none;
		color:#fff;
		border-radius:5px;
		text-decoration: none;
		padding:10px;
	    }
	    .myBox {
		position: relative;
		top:2em;
		left:-30em;
	    }
	    
	    #form1{
		display:flex; 
		position: fixed;
		top:12em;
		left:-16em;
		margin-left: 36em;
		margin-top: 5em;
	    }
	    #form2{
		display:flex; 
		position: fixed;
		top:17em;
		left:-16em;
		margin-left: 36em;
		margin-top: 5em;
	    }
	    img{
		position: fixed;
		top:7em;
		left:20em;
	    }
	    .flabel1{
		position: fixed;
		top:24em;
		left:55em;
	    }
	    .flabel3{
		position: fixed;
		top:18em;
		left:55em;
	    }
	    .flabel2{
		position: fixed;
		top:30em;
		left:55em;
	    }
	    .flabel4{
		position: fixed;
		top:10em;
		left:33em;
	    }
	    
	</style>
    </head>
	<%
	     response.setHeader("Cache-Control","no-cache,no-store,must-revalidate"); //HTTP 1.1
	    
	    response.setHeader("Pragma","no-cache"); //HTTP1.0
	    response.setHeader("Expires","0"); //Proxies
	    if(session.getAttribute("username")==null){response.sendRedirect("login.jsp");}
	%>
	<%
	   
	    String username=(String)session.getAttribute("username");
	  String success = (String)session.getAttribute("success");
	  Map<String,String>error=(Map)session.getAttribute("error");
	  
	  String dsuccess = (String)session.getAttribute("dsuccess");
	  Map<String,String>errors=(Map)session.getAttribute("errors");
	    pageContext.setAttribute("error", error);
            pageContext.setAttribute("success", success);
	    
	    pageContext.setAttribute("errors", errors);
            pageContext.setAttribute("dsuccess", dsuccess);
	    
	    session.removeAttribute("error");
            session.removeAttribute("success");
	    
	    session.removeAttribute("errors");
            session.removeAttribute("dsuccess");
	     
	%>
    <body>
	<div class="container">
	 <div class="user">
		<span>Username: <%= username %> </span>	
	</div>
	<div class="myBox">
	    
	<label style="color: red;font-style: italic;font-size: 10px;"class="flabel3">${error["keyIn"]}</label><br>
	<label style="color: red;font-style: italic;font-size: 10px;"class="flabel3">${errors["dkeyIn"]}</label><br>
	<label style="color: red;font-style: italic;font-size: 10px;"class="flabel3">${errors["global"]}</label><br>
	<label style="color: green;font-style: italic;font-size: 10px;"class="flabel4">${success}</label><br>
	<label style="color: green;font-style: italic;font-size: 10px;"class="flabel4">${dsuccess}</label><br>
	<form action="encrypt" id="form1" enctype="multipart/form-data" method="post" >
	    
	    <p><img id="output" width="100" /></p>
	    <input type="file"  accept="image/*" name="image" id="file"  onchange="loadFile(event)" required>
	    <input type="text" name="keyIn" placeholder="enter your encryption key.." required><button>encrypt</button> <br><label class="flabel1" style="color:red;font-style: italic;font-size: 10px;">${error["key"]}</label>
	    
	</form>
	    
	<form action="decrypt" id="form2"enctype="multipart/form-data" method="post">
	    <input type="file"  accept="image/*" name="image" id="file"  onchange="loadFile(event)" required>
	    <input type="text" name="key" placeholder="enter your decryption key.." required><button>decrypt</button><br><label class="flabel2" style="color:red;font-style: italic;font-size: 10px;">${errors["key"]}</label>
	    
	</form>
	    <form action="logout"><button class="btnBack">Logout</button></form> 

	    <script>
	    var loadFile = function(event) {
		    var image = document.getElementById('output');
		    image.src = URL.createObjectURL(event.target.files[0]);
	    };
	    </script>
	</div>
	</div>
    </body>
</html>
